FROM openjdk:8-jre-alpine
ENV artifact spring-petclinic-2.1.0.BUILD-SNAPSHOT.jar
WORKDIR /app
RUN pwd
COPY target/${artifact} /app
EXPOSE 8080
CMD exec java -jar ${artifact}
